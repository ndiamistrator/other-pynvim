"""Msgpack-rpc subpackage.

This package implements a msgpack-rpc client. While it was designed for
handling some Nvim particularities(server->client requests for example), the
code here should work with other msgpack-rpc servers.
"""
from pynvim.msgpack_rpc.async_session import AsyncSession
from pynvim.msgpack_rpc.event_loop import EventLoop
from pynvim.msgpack_rpc.msgpack_stream import MsgpackStream
from pynvim.msgpack_rpc.session import ErrorResponse, Session
from pynvim.util import get_client_info

from pynvim.msgpack_rpc.event_loop.base import Timeout


__all__ = ('tcp_session', 'socket_session', 'stdio_session', 'child_session',
           'ErrorResponse')

__all__ = tuple((*__all__, 'Timeout'))


def session(transport_type='stdio', *args, timeout=None, **kwargs):
    loop = EventLoop(transport_type, *args, timeout=timeout, **kwargs)
    msgpack_stream = MsgpackStream(loop)
    async_session = AsyncSession(msgpack_stream)
    session = Session(async_session)
    session.request(b'nvim_set_client_info',
                    *get_client_info('client', 'remote', {}), async_=True)
    return session


def tcp_session(address, port=7450, *, timeout=None):
    """Create a msgpack-rpc session from a tcp address/port."""
    return session('tcp', address, port, timeout=timeout)


def socket_session(path, *, timeout=None):
    """Create a msgpack-rpc session from a unix domain socket."""
    return session('socket', path, timeout=timeout)


def stdio_session(*, timeout=None):
    """Create a msgpack-rpc session from stdin/stdout."""
    return session('stdio', timeout=timeout)


def child_session(argv, *, timeout=None):
    """Create a msgpack-rpc session from a new Nvim instance."""
    return session('child', argv, timeout=timeout)
