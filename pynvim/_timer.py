import threading
import logging


logger = logging.getLogger(__name__)
debug = logger.debug


__all__ = 'Timer', 'Timeout'


class Timeout(Exception):
    pass


class Timer:
    ''' Timer with trigger & cancel synchronization. (WIP)

        Optionally invokes a callback on trigger.
        Timeout can be a callable to return a timeout.

        ie.

        from time import time

        t = Timer(time() + 30.0, f, args=None, kwargs=None)
        t.start()
        t.cancel()  # Stop the timer's action if it's still waiting.
                    # Raise Timeout otherwise.
    '''

    Timeout = Timeout

    class Status(Exception):
        def __init__(S, *args, **kwargs):
            super().__init__(*args, **kwargs)
            debug(f'status: {S!r}')
        def __str__(S):
            return S.__class__.__name__

    class Initializing(Status): pass
    class Initialized(Status): pass

    class Start(Initialized): pass

    class Starting(Start): pass
    class Started(Start): pass

    class Running(Started): pass

    class Trigger(Started): pass
    class Cancel(Started): pass

    class Triggering(Trigger, Running): pass
    class Cancelling(Cancel, Running): pass

    class Finished(Started): pass

    class Triggered(Trigger, Finished): pass
    class Cancelled(Cancel, Finished): pass

    class Error(Finished, RuntimeError):
        @property
        def exception(S):
            return S.args[0]

    class TriggerError(Triggered, Error): pass
    class StartError(Error): pass
    class UnexpectedError(Error): pass

    _callback = None

    def __init__(S, callback=None, daemon=True):
        S._status = S.Initializing()

        if callback is not None:
            assert callable(callback), callback
            S._callback = callback

        S._started = threading.Event()
        S._cancel = threading.Event()

        S._lock = threading.Lock()
        S._thread = threading.Thread(target=S._run, daemon=daemon)

        S._status = S.Initialized()

    @property
    def status(S):
        return S._status

    def start(S, timeout):
        assert not isinstance(S._status, S.Start)
        debug('start: locking...')
        with S._lock:
            assert not isinstance(S._status, S.Start)
            S._timeout = timeout
            S._status = S.Starting()
            S._thread.start()
            S._started.wait()
            if isinstance(S._status, S.Error):
                raise S._status.exception
        return S

    def wait(S, timeout=None):
        assert isinstance(S._status, S.Started)
        S._thread.join(timeout)
        return S

    def check(S, timeout=None):
        if isinstance(S.wait(timeout)._status, S.Error):
            raise S._status.exception

    def cancel(S):
        assert isinstance(S._status, S.Started)
        if isinstance(S._status, S.Trigger):
            raise S.Timeout(S)
        elif not isinstance(S._status, S.Running):
            return S
        debug('cancel: locking...')
        with S._lock:
            if isinstance(S._status, S.Trigger):
                raise S.Timeout(S)
            elif not isinstance(S._status, S.Running):
                return S
            S._status = S.Cancelling()
            S._cancel.set()
        return S

    def _run(S):
        try:
            try:
                if callable(S._timeout):
                    _timeout = S._timeout()
                    timeout = float(_timeout)
                else:
                    timeout = float(S._timeout)
            except Exception as E:
                S._status = S.StartError(E)
                return
            else:
                S._status = S.Running()
            finally:
                S._started.set()

            if timeout > 0:
                S._cancel.wait(timeout)

            if isinstance(S._status, S.Cancelling):
                S._status = S.Cancelled()
                return

            debug('run: locking...')
            with S._lock:
                if isinstance(S._status, S.Cancelling):
                    S._status = S.Cancelled()
                    return
                S._status = S.Triggering()
                if S._callback is not None:
                    try:
                        S._callback()
                    except Exception as E:
                        S._status = S.TriggerError(E)
                        return
                S._status = S.Triggered()
        except Exception as E:
            S._status = S.UnexpectedError(E)
            raise
        finally:
            debug('run: finished')

    def __str__(S):
        status = None
        serror = ''

        try:
            status = S._status
        except Exception as E:
            status = 'unknown'

        return f'<{S.__class__.__name__} {status}>'

    __repr__ = __str__


# _tests = []
# def _test(f):
#     _tests.append((f.__name__, f))
# 
# def _test_0():
#     timeout = Timer(lambda:debug('callback'))
#     timeout.start(1)
#     timeout.wait(2)
# 
# 
# def _test_start_assertion():
#     timeout = Timer(lambda:debug('callback'))
#     timeout.start(1)
#     try:
#         timeout.start(1)
#     except Exception as E:
#         debug('expected exception', E, repr(E))
#     timeout.wait(2)
# 
# def _test_wait_assertion():
#     timeout = Timer(lambda:debug('callback'))
#     try:
#         timeout.wait(1)
#     except Exception as E:
#         debug('expected exception', E, repr(E))
# 
# def _test_cancel_assertion():
#     timeout = Timer(lambda:debug('callback'))
#     try:
#         timeout.cancel()
#     except Exception as E:
#         debug('expected exception', E, repr(E))
# 
# 
# def _test_cancel():
#     timeout = Timer(lambda:debug('callback'))
#     timeout.start(1)
#     timeout.cancel()
#     timeout.wait()
# 
# def _test_cancel_triggered():
#     timeout = Timer(lambda:debug('callback'))
#     timeout.start(1)
#     timeout.wait(2)
#     try:
#         timeout.cancel()
#     except Exception as E:
#         debug('expected exception', E, repr(E))
# 
# def _test_cancel_on_callback():
#     timeout = Timer(lambda:(debug('callback'), timeout.cancel()))
#     timeout.start(1)
#     timeout.wait(2)
#     try:
#         timeout.cancel()
#     except Exception as E:
#         debug('expected exception', E, repr(E))
# 
# def _test_wait_on_callback():
#     timeout = Timer(lambda:(debug('callback'), timeout.wait()))
#     timeout.start(1)
#     timeout.wait(2)
# 
# def _test_check_ok():
#     timeout = Timer()
#     timeout.start(1)
#     timeout.check(2)
# 
# def _test_check_error():
#     timeout = Timer(lambda:asdf)
#     timeout.start(1)
#     try:
#         timeout.check(2)
#     except Exception as E:
#         debug('expected exception', E, repr(E))
# 
# 
# if __name__ == '__main__':
#     import sys
# 
#     if not _tests:
#         _tests = [(name, test) for name, test in tuple(globals().items())
#                                if name.startswith('_test_')]
#     for name, test in _tests:
#         def debug(*args):
#             print(f'{name}:', *args, file=sys.stderr)
#         debug('starting...')
#         test()
#         print(file=sys.stderr)
# 
#     if _tests:
#         print('done testing', file=sys.stderr)
